﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] matrix88, transponMatrix88;
            bool[] equalRows;
            bool[] negativeRows;
            int n = 3;
            bool b = true;
            bool pointNeg = false;
            int tmp;

            matrix88 = new int[,] { { 1, 2, 3 }, { 2, 3, 1 }, { 3, 1, -2 } };
            transponMatrix88 = new int[n, n];
            equalRows = new bool[n];
            negativeRows = new bool[n];

            // initialization
            //for (int i = 0; i < n; i++)
            //{
            //    for (int j = 0; j < n; j++)
            //    {
            //        Console.WriteLine("Input element {0} {1}", i, j);
            //        matrix88[i, j] = Convert.ToInt32(Console.ReadLine());

            //    }
            //}

            // transposition matrix

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    transponMatrix88[i, j] = matrix88[j, i];
                    Console.Write(transponMatrix88[i, j] + " ");
                }
                Console.WriteLine();
            }

            // finding equals rows
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (transponMatrix88[i, j] != matrix88[i, j])
                    {
                        b = false;
                        break;
                    }
                }

                equalRows[i] = b;
                b = true;

            }

            Console.WriteLine("Numbers of elements:");
            for (int i = 0; i < n; i++)
            {
                //Console.Write(equalRows[i] + " ");
                if (equalRows[i] == true)
                    Console.WriteLine("Row " + i + " is the same as " + i + " column");
            }

            // finding rows with negative elements
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (matrix88[i, j] < 0)
                    {
                        pointNeg = true;
                        break;
                    }
                }
                negativeRows[i] = pointNeg;
                pointNeg = true;
                if (negativeRows[i] == true)
                Console.WriteLine("Row " + i + " has negative element");
                Console.Read();
            }
        }
    }
}
